import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import VueTimers from 'vue-timers';
import interceptorsSetup from './services/interceptors';

interceptorsSetup();

Vue.config.productionTip = false;
Vue.use(VueTimers);

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
