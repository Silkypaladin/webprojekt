import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Register from '../views/Register.vue';
import Lobby from '../views/Lobby.vue';
import Profile from '../views/Profile.vue';
import CreateQuestion from '../views/CreateQuestion';
import CreateQuiz from '../views/CreateQuiz';
import JoinGame from '../views/JoinGame';
import Summary from '../views/Summary';
Vue.use(VueRouter);
const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/join-game',
    name: 'JoinGame',
    component: JoinGame,
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
  },
  {
    path: '/lobby',
    name: 'Lobby',
    component: Lobby,
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
  },
  {
    path: '/create-question',
    name: "CreateQuestion",
    component: CreateQuestion,
  },
  {
    path: '/create-quiz',
    name: "CreateQuiz",
    component: CreateQuiz,
  },
  {

    path: '/summary',
    name: "Summary",
    component: Summary,
  },
  {
    path: '/game',
    name: 'Game',
    component: () => import('../views/Game.vue'),
  },
];

const router = new VueRouter({
  mode:'history',
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.name !== 'Home' && !sessionStorage.getItem('token') && to.name !== 'Register') {
  next({ name: 'Home' })
  }
  else next();
});

export default router;
