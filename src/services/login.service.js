import axios from 'axios';
import store from '../store';
import router from '../router';
class LoginService {

  login(username, password) {
    return axios.post(`${process.env.VUE_APP_API_URL}/api/authenticate`,
      {username: username, password: password, rememberMe: false})

  };
  register(login, email, password, firstname, lastname){
    return axios.post(`${process.env.VUE_APP_API_URL}/api/register`,
      {login: login,email: email, password: password, firstname: firstname, lastname: lastname})
  }

  retrieveAccount() {
    return new Promise(resolve => {
      axios
        .get(`${process.env.VUE_APP_API_URL}/api/account/currentUser`)
        .then(response => {
          store.commit('authenticate');
          const account = response.data;
          if (account) {
            store.commit('authenticated', account);
          } else {
            store.commit('logout');
            router.push('/', () => {});
          }
          resolve(true);
        })
        .catch(() => {
          store.commit('logout');
          resolve(false);
        });
    });
  }
}

export default new LoginService();
