import io from 'socket.io-client';

class GameService {
  socket = io("http://localhost:3000");
  roomId = '';
  newRoom(username) {
    if(this.socket.disconnected){
      this.socket.connect();
    }
    this.socket.emit('newRoom', username);
  }

  joinRoom(roomId, username) {
    this.socket.emit('joinRoom', roomId, username);
  }
  hostDisconnected(roomId) {
    this.socket.emit('hostDisconnected', roomId);
  }
  userLeft() {
    this.socket.disconnect();
  }
  beginGame(roomId, quizId) {
    this.socket.emit('beginGame', roomId, quizId);
  }
  finishGame(roomId, userLogin, score) {
    this.socket.emit('finishGame', roomId, userLogin, score);
  }
}

export default new GameService();
