import axios from 'axios';

class QuizService {
  createNew(title, description, categoryId) {
    return axios.post(`${process.env.VUE_APP_API_URL}/api/quizzes`,
      {title: title, description: description, categoryId: categoryId})
  };

  getUserQuizzes() {
    return axios.get(`${process.env.VUE_APP_API_URL}/api/quizzes/currentUser`);
  };

  getAllQuizzes() {
    return axios.get(`${process.env.VUE_APP_API_URL}/api/quizzes`);
  };

  getUserScores() {
    return axios.get(`${process.env.VUE_APP_API_URL}/api/scores/currentUser`)
  };

  addQuizQuestions(quizId, questions) {
    return axios.post(`${process.env.VUE_APP_API_URL}/api/quizzes/${quizId}/questionslist`,
    questions);
  }

  getQuiz(quizId) {
    return axios.get(`${process.env.VUE_APP_API_URL}/api/quizzes/${quizId}/questions`);
  }
  saveQuizScore(place, quizId) {
    return axios.post(`${process.env.VUE_APP_API_URL}/api/scores`,
      {place: place, quizId: quizId });
  }
}

export default new QuizService();
