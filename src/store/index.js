import Vue from 'vue';
import Vuex from 'vuex';
import accountStore from './accountStore.js';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    accountStore: accountStore,
  },
});
