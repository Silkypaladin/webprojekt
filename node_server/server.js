const app = require("express")();
const http = require("http").Server(app);
const io = require("socket.io")(http, {
  cors: {
    origin: "http://localhost:9000",
    methods: ["GET", "POST"]
  }
});

let rooms = {};
let games = {};
let finishedGames = {};
let shortid = require('shortid');

io.on("connection", socket => {
  socket.on('newRoom',username => {
    let roomId = shortid.generate();
    socket.nickname = {roomId: roomId, username: username};
    rooms[roomId] = [username];
    socket.join(roomId);
    console.log(`New room has been created by: ${username} RoomId: ${roomId}`);
    io.to(roomId).emit('newRoomId', roomId, rooms[roomId]);
  });
  socket.on('joinRoom', (roomId, username) =>{
    if(roomId in rooms) {
      socket.join(roomId);
      socket.nickname = {roomId: roomId, username: username};
      rooms[roomId].push(username);
      io.to(roomId).emit('newUserJoined', rooms[roomId]);
      socket.emit('joinSuccessful', rooms[roomId]);
    }
    else{
      socket.emit('joinError');
    }
  });

  socket.on('hostDisconnected', (roomId) => {
    console.log(`Host has left room: ${roomId}`);
    io.to(roomId).emit('hostDisconnected');
  });

  socket.on('beginGame', (roomId, quizId) => {
    console.log(`Game room ${roomId} started their game.`);
    io.to(roomId).emit('beginGame', quizId);
  });

  socket.on('finishGame', (roomId, userLogin, score) =>{
    if (!games[roomId]) {
      games[roomId] = [{userLogin: userLogin, score: score}];
      finishedGames[roomId] = false;
    } else {
      const found = games[roomId].some((player) => player.userLogin === userLogin);
      if (!found) {
        games[roomId].push({userLogin: userLogin, score: score});
      }
    }
    if (games[roomId].length === rooms[roomId].length && !finishedGames[roomId]) {
      finishedGames[roomId] = true;
      games[roomId].sort((a, b) => (a.score > b.score) ? -1 : (b.score > a.score) ? 1 : 0);
      console.log(`Game room ${roomId} is finished.`);
      io.to(roomId).emit('finishGame', games[roomId]);
    }
  });

  socket.on("disconnect", () => {
    let userData = socket.nickname;
    if (userData) {
      const userIndex = rooms[userData['roomId']].indexOf(userData['username']);
      if (userIndex > -1) {
        console.log(`User ${userData['username']} disconnected from ${userData['roomId']}`);
        rooms[userData['roomId']].splice(userIndex, 1);
        if (rooms[userData['roomId']].length === 0) {
          console.log(`Room ${userData['roomId']} destroyed`);
          delete rooms[userData['roomId']];
        }
      }
      io.emit("userLeft", rooms[userData['roomId']]);
    }
  });
});

http.listen(process.env.PORT || 3000, () => {
  console.log("Listening on port %s", process.env.PORT || 3000);
});
